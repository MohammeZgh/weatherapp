import { shallowMount } from '@vue/test-utils';
import LocationInput from '@/components/LocationInput.vue';

describe('LocationInput.vue', () => {
  let wrapper = null;
  beforeEach(() => {
    wrapper = shallowMount(LocationInput);
  });
  afterEach(() => {
    wrapper.destroy();
  });
  it('should initialize correctly', () => {
    expect(wrapper.vm).toBeTruthy();
  });
  it('should emit event when sendInput is called', () => {
    wrapper.setData({ location: 'Paris' });
    wrapper.vm.sendInput();
    expect(wrapper.emitted('location')).toBeTruthy();
    expect(wrapper.emitted('location').length).toBe(1);
    expect(wrapper.emitted('location')[0][0]).toMatch('Paris');

    // Input must be cleared after event
    expect(wrapper.vm.location).toMatch('');
  });
});
