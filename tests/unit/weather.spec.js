import { shallowMount } from '@vue/test-utils';
import Weather from '@/components/Weather.vue';

describe('Weather.vue', () => {
  let wrapper = null;
  beforeEach(() => {
    wrapper = shallowMount(Weather, {
      propsData: {
        name: 'Paris',
        temperature: 19.59,
        sunrise: 1594699340,
        sunset: 1594756214,
        humidity: 52,
        weather: 'Rain',
        weather_description: 'light rain',
        weather_icon: '10d',
      },
    });
  });
  afterEach(() => {
    wrapper.destroy();
  });
  it('should initialize correctly', () => {
    expect(wrapper.vm).toBeTruthy();
  });
  it('should render props correctly', async () => {
    await wrapper.setProps({
      weather: {
        name: 'Paris',
        temperature: 19.59,
        sunrise: 1594699340,
        sunset: 1594756214,
        humidity: 52,
        weather: 'Rain',
        weather_description: 'light rain',
        weather_icon: '10d',
      },
    });
    // Test props
    expect(wrapper.props().weather.name).toMatch('Paris');
    expect(wrapper.props().weather.temperature).toEqual(19.59);
    expect(wrapper.props().weather.sunrise).toEqual(1594699340);
    expect(wrapper.props().weather.sunset).toEqual(1594756214);
    expect(wrapper.props().weather.humidity).toEqual(52);
    expect(wrapper.props().weather.weather).toMatch('Rain');
    expect(wrapper.props().weather.weather_description).toMatch('light rain');
    expect(wrapper.props().weather.weather_icon).toMatch('10d');

    // Test props render
    expect(wrapper.findAll('h6').length).toEqual(2);
    expect(wrapper.findAll('h6').at(0).text()).toMatch('Paris');
    expect(wrapper.findAll('h6').at(1).text()).toMatch('Tue Jul 14 2020');

    expect(wrapper.findAll('h1').at(0).text()).toMatch('20° C');

    expect(wrapper.findAll('span').at(0).text()).toMatch('Rain');
    expect(wrapper.findAll('span').at(1).text()).toMatch('light rain');
    expect(wrapper.findAll('span').at(2).text()).toMatch('52');
    expect(wrapper.findAll('span').at(3).text()).toMatch('4h : 2');
    expect(wrapper.findAll('span').at(4).text()).toMatch('19h : 50');
  });
});
