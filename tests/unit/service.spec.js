import getWeather from '../../src/service';

const weatherData = {
  coord: { lon: 2.35, lat: 48.85 },
  weather: [{
    id: 804, main: 'Clouds', description: 'overcast clouds', icon: '04d',
  }],
  base: 'stations',
  main: {
    temp: 15.56,
    feels_like: 13.47,
    temp_min: 13.89,
    temp_max: 16.67,
    pressure: 1018,
    humidity: 58,
  },
  visibility: 10000,
  wind: { speed: 2.1, deg: 330 },
  clouds: { all: 87 },
  dt: 1594703382,
  sys: {
    type: 1,
    id: 6550,
    country: 'FR',
    sunrise: 1594699340,
    sunset: 1594756214,
  },
  timezone: 7200,
  id: 2988507,
  name: 'Paris',
  cod: 200,
};

global.fetch = jest.fn(() => Promise.resolve({
  json: () => Promise.resolve(weatherData),
}));

describe('Call Open Weather API', () => {
  it('should return current weather', async () => {
    const data = await getWeather('weather?city=Paris');
    expect(data).toMatchObject(weatherData);
  });
  it('should handle exceptions', async () => {
    fetch.mockImplementationOnce(() => Promise.reject(new Error('API failure')));

    setTimeout(async () => {
      const data = await getWeather('weather?q=Paris');
      expect(data).toThrowError('API failure');
    }, 0);
  });
});
