export default async function getPosition() {
  return new Promise((resolve, reject) => {
    if (!navigator.geolocation) {
      reject(new Error('geolocation not supported'));
    }
    navigator.geolocation.getCurrentPosition((position) => {
      resolve(position.coords);
    }, (err) => {
      reject(err);
    });
  });
}
